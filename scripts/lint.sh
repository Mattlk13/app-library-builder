#!/bin/bash
black --fast -t py37 -t py38 -l 100 scripts/ chinook/ tests/
flake8 scripts/ chinook/ tests/
