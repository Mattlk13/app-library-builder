# MariaDB

MariaDB Server est l'un des serveurs de base de données les plus populaires au monde. Il est créé par les développeurs de MySQL et garanti pour rester libre. Les utilisateurs notables incluent Wikipedia, WordPress.com et Google.

MariaDB transforme les données en informations structurées dans un large éventail d'applications, allant de la banque aux sites Web. Conçue à l'origine pour remplacer MySQL, MariaDB est utilisée car il est rapide, évolutif et robuste, avec un riche écosystème de moteurs de stockage, de plugins et de nombreux autres outils qui la rendent très polyvalente pour une grande variété de cas d'utilisation.

MariaDB est développé en tant que logiciel libre et en tant que base de données relationnelle, il fournit une interface SQL pour accéder aux données. Les dernières versions de MariaDB incluent également des fonctionnalités SIG et JSON.

![MariaDB logo](https://raw.githubusercontent.com/linuxserver/docker-templates/master/linuxserver.io/img/mariadb-git.png)