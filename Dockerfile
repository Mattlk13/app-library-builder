FROM python:3.9-bullseye

USER root

ADD requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt

ADD scripts/ /scripts/
WORKDIR /scripts/

RUN mkdir /src/
ADD chinook/ /src/chinook/
ADD setup.py /src/
RUN pip3 install /src/

CMD applib-builder

